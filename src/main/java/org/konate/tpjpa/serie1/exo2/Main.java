package org.konate.tpjpa.serie1.exo2;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.konate.tpjpa.serie1.exo2.model.Eleve;
import org.konate.tpjpa.serie1.exo2.model.Instruments;
import org.konate.tpjpa.serie1.exo2.model.Professeur;

public class Main {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = 
				Persistence.createEntityManagerFactory("jpatest");

		EntityManager entityManager = entityManagerFactory.createEntityManager();

		/*Calendar calendar = new GregorianCalendar(1980, 5, 20);
		Date date = calendar.getTime();

		UserFactory uf = new UserFactory(entityManager);
		User user = uf.createUser("Kylian", "MBAPPE", date);

		System.out.println(user);
		System.out.println(uf.find(1));
		uf.delete(1);*/
		//String eleveClassName = Eleve.class.getName();
		//String qlString = "SELECT new " + eleveClassName + "(e.nom, e.age) FROM Eleve e";
		/*String qlString = "select eleve from Eleve eleve where eleve.nom = 'Albert'";
		Query query = entityManager.createQuery(qlString);
		List<Eleve> result = query.getResultList() ;
		System.out.println(result);*/
		
		String qlString = "UPDATE school.hibernate_sequence SET next_val=15";
		entityManager.createNativeQuery(qlString);
		
		/*EleveFactory ef = new EleveFactory(entityManager);
		ef.delete(13);
		ef.delete(11);
		ef.delete(9);**/
		Instruments instrument = new Instruments();
		instrument.setNom("Saxophone");
		instrument.setLocation("O");
		instrument.setPrix(1000);
		instrument.setPrixCours(15);
	
		Professeur victor = new Professeur("victor", 38);
		victor.setInstrument(instrument);
		
		entityManager.getTransaction().begin();
		entityManager.persist(victor);
		entityManager.getTransaction().commit();
		
		Eleve adrien = new Eleve("Adrien", 19);
		entityManager.getTransaction().begin();
		entityManager.persist(adrien);
		entityManager.getTransaction().commit();
		
	}
}








